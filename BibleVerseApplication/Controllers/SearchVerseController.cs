﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BibleVerseApplication.Models;
using PagedList;
using PagedList.Mvc;

namespace BibleVerseApplication.Controllers
{
    public class SearchVerseController : Controller
    {
        private BibleVerseApplicationContext db = new BibleVerseApplicationContext();

        // GET: SearchVerse
        [Authorize]
        public ActionResult Index(string searchBy, string search, int? pageNumber)
        {
            if (searchBy == "BookName")
            {
                return View(db.BibleUserEntries.Where(x => x.BookSelection.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3 ));
            }
            else if (searchBy == "Testament")
            {
                return View(db.BibleUserEntries.Where(x => x.TestamentSelection.ToString() == search || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
            }
            else if (searchBy == "VerseNumber")
            {
                return View(db.BibleUserEntries.Where(x => x.VerseNumber.ToString() == search || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
            }
            else 
            {
                return View(db.BibleUserEntries.Where(x => x.ChapterNumber.ToString() == search || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
            }
        }

        // GET: SearchVerse/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleUserEntry bibleUserEntry = db.BibleUserEntries.Find(id);
            if (bibleUserEntry == null)
            {
                return HttpNotFound();
            }
            return View(bibleUserEntry);
        }
        // GET: SearchVerse/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleUserEntry bibleUserEntry = db.BibleUserEntries.Find(id);
            if (bibleUserEntry == null)
            {
                return HttpNotFound();
            }
            return View(bibleUserEntry);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleUserEntry bibleUserEntry = db.BibleUserEntries.Find(id);
            if (bibleUserEntry == null)
            {
                return HttpNotFound();
            }
            return View(bibleUserEntry);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BibleUserEntry bibleUserEntry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bibleUserEntry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "BibleUserEntries");
            }
            return View(bibleUserEntry);
        }

        // POST: SearchVerse/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BibleUserEntry bibleUserEntry = db.BibleUserEntries.Find(id);
            db.BibleUserEntries.Remove(bibleUserEntry);
            db.SaveChanges();
            return RedirectToAction("Index", "BibleUserEntries");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
